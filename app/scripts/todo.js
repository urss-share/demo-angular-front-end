'use strict';

var todo = angular.module('todo', ['ngResource']);

todo.controller('TodoController', function($scope, TodoService) {
	$scope.todos = TodoService.query();

	$scope.save = function(form) {
		var newTodo = {
			title: form.title,
			description: form.description
		}
		console.log(newTodo);
		newTodo = TodoService.save(newTodo);
		$scope.todos.unshift(newTodo);
	};
});

todo.factory('TodoService', ['$resource', function ($resource) {

	const SERVICE_URL_BASE = 'http://localhost\\:8080/service/todo';

	return $resource(SERVICE_URL_BASE);

}]);