'use strict';

var app = angular.module('messageBoard');

app.controller('MsgBoardCtrl', ['$scope', '$rootScope', 'UserService', 'MessageBoardService', function($scope, $rootScope, UserService, MessageBoardService) {
		
	$scope.title = 'Message Board';
	$scope.messages = MessageBoardService.query();
	$scope.loggedinuser = UserService.getUser();
	$rootScope.loading = true;

	function Message(author, content, ancestors) {
		//var id;
		this.author = author;
		//var parentid;
		this.message = content;
		//var created;
		this.ancestors = ancestors;
		this.datetime = null;
	}

	this.createMessage = function (author, content) {
		return new Message(author, content);
	};

	$scope.newSubItem = function (scope) {
        var parrent = scope.$modelValue;
        if (scope.newMsg) {
        	if (parrent.ancestors === null) {
        		parrent.ancestors = [];
        	}
	        parrent.ancestors.push(new Message($scope.loggedinuser, scope.newMsg));
        	scope.newMsg = null;
    	}
     };

	$scope.saveMessage = function(scope) {

		if (scope) {
			//$scope.addedMessage = createMessage()
			$scope.addedMessage = {msgid:null, author: $scope.loggedinuser, parentid: null, msg: scope, datetime: 'just now', nodes: []};
			$scope.messages.unshift($scope.addedMessage);
		}	
	};


}]);
