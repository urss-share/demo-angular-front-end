'use strict';

/**
 * @ngdoc overview
 * @name demoApp
 * @description
 * # demoApp
 *
 * Main module of the application.
 */
var demoApp = angular
  .module('demoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.tree',
    'time',
    'messageBoard',
    'todo'
  ]);

demoApp.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/msgboard', {
        templateUrl: 'views/msgboard.html',
        controller: 'MsgBoardCtrl',
        controllerAs: 'msgBoard'
      })
      .when('/todo', {
        templateUrl: 'views/todo.html',
        controller: 'TodoController',
        controllerAs: 'todoCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: ''
      })
      .otherwise({
        redirectTo: '/'
      });
  });

demoApp.factory('resourceInterceptor', ['$rootScope',
  function ($rootScope) {
    return {
      request: function (config) {
        // This function isn't executed at all?
        $rootScope.loading = true;
        return config;
      },
      response: function (response) {
        $rootScope.loading = false;
        return response;
      },
      responseError: function (response) {
        alert(response.status + ' - ' + response.statusText);
        return response;
      }
    };
  }]);

demoApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('resourceInterceptor');
}]);

demoApp.factory('UserService', function() {
    var user = 'zsizsik';
    var service = {
      getUser: function() {
        return user;
      }
    };
      return service;
  });
