'use strict';

var time = angular.module('time', ['ngResource']);

time.factory('TimeService', function($resource) {
	return $resource('http://10.71.247.26\\:8080/service/time/getTime');
});