'use strict';

var messageBoard = angular.module('messageBoard', ['ngResource']);

messageBoard.factory('MessageBoardService', ['$resource', function($resource) {
	
	const SERVICE_URL_BASE = 'http://localhost\\:8080/service/message';
	
	return $resource(SERVICE_URL_BASE + '/:id', {tokmindegy: '@id'}, {
		query: { method: 'GET', isArray: true, url: SERVICE_URL_BASE + '/all' }
	});

}]);


		// ,
		// get: { method: 'GET', isArray: false, url: SERVICE_URL_BASE + '/:id' }
