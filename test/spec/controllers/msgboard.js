'use strict';

describe('Controller: MsgBoardCtrl', function () {

  // load the controller's module
  beforeEach(module('urssDemoApp'));

  var MsgBoardCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MsgBoardCtrl = $controller('MsgBoardCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('MsgBoardCtrl should not be null.', function () {
    expect(MsgBoardCtrl).not.toBe(null);
  });

  it('MsgBoardCtrl.createMessage should not be null.', function () {
    expect(MsgBoardCtrl.createMessage).not.toBe(null);
  });

  it('MsgBoardCtrl.createMessage("someAuthor") should not be null.', function () {
    expect(MsgBoardCtrl.createMessage('someAuthor')).not.toBe(null);
  });

  it('MsgBoardCtrl.createMessage("someAuthor") should not be null.', function () {
    expect(MsgBoardCtrl.createMessage('someAuthor').author).not.toBe(null);
  });

  it('MsgBoardCtrl.createMessage("someAuthor") should not be null.', function () {
    expect(MsgBoardCtrl.createMessage('someAuthor').author).toBe('someAuthor');
  });

  it('Calling createMessage tow time with diffrent author should create two differnt Message objects.', function () {
    var msg1 = MsgBoardCtrl.createMessage('someAuthor');
    var msg2 = MsgBoardCtrl.createMessage('otherAuthor');
    expect(msg1.author).toBe('someAuthor');
    expect(msg2.author).toBe('otherAuthor');
  });

  it('Calling MsgBoardCtrl.Message("someAuthor") should result in TypeError.', function () {
    expect(MsgBoardCtrl.Message).toBeUndefined();
  });


});
